﻿$(document).ready(function () {


    $("input:radio[name=color]").change(function () {
        var selectedColor = $('input[type="radio"]:checked').val();
        $("#selectedColor").text(selectedColor);

    });
    $("input:radio[name=size]").change(function () {
        var selectedSize = $('input:radio[name=size]:checked').val();
        $("#selectedSize").text(selectedSize);
    });

    $(".submit-add-btn").on('click', function (e) {
        var current = $(this).closest('div').find('span').text();
        let currentCount = parseInt(current);

        $.ajax({
            type: "POST",
            url: '/cart/Add',
            data: { id: $(this).val(), count: 1 },
        }).fail(function (data) {
            if (data.status == 401) {
                window.location.replace("/account/login");
            }
        })
            .done(function (data) {
                console.log(data.status);
                $.get("/cart/Count", function (data, status) {
                    if (data > 0) {
                        $("#cart-count").addClass("cart-count");
                        $("#cart-count").text(data)
                    }
                    else {
                        $("#cart-count").removeClass("cart-count");
                    }
                })
            });


    });


});

