﻿namespace OnlineShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewSkirtsImagesAreAdded : DbMigration
    {
        public override void Up()
        {
            Sql(@"insert into [Image](Link,itemId,isMain) values
                    ('https://www.upsara.com/images/e602279_1.jpg',11,0),
                    ('https://www.upsara.com/images/p035452_2.jpg',11,1),
                    ('https://www.upsara.com/images/m584283_3.jpg',11,0),
                    ('https://www.upsara.com/images/l496507_4.jpg',11,0),
                    
                    ('https://www.upsara.com/images/f236266_1.jpg',12,0),
                    ('https://www.upsara.com/images/k553036_2.jpg',12,1),
                    ('https://www.upsara.com/images/t684446_3.jpg',12,0),
                    ('https://www.upsara.com/images/y374683_4.jpg',12,0),
                    
                    
                    ('https://www.upsara.com/images/a990921_1.jpg',13,0),
                    ('https://www.upsara.com/images/v878024_2.jpg',13,1),
                    ('https://www.upsara.com/images/p662750_3.jpg',13,0),
                    ('https://www.upsara.com/images/f912006_4.jpg',13,0),
                    
                    ('https://www.upsara.com/images/d132882_1.jpg',14,0),
                    ('https://www.upsara.com/images/o983538_2.jpg',14,1),
                    ('https://www.upsara.com/images/y163341_3.jpg',14,0),
                    ('https://www.upsara.com/images/l117686_4.jpg',14,0),
                    
                    ('https://www.upsara.com/images/p374771_1.jpg',15,0),
                    ('https://www.upsara.com/images/k886667_2.jpg',15,0),
                    ('https://www.upsara.com/images/d114898_3.jpg',15,0),
                    ('https://www.upsara.com/images/f513720_4.jpg',15,1),
                    
                    ('https://www.upsara.com/images/a852514_1.jpg',16,0),
                    ('https://www.upsara.com/images/x659136_2.jpg',16,1),
                    ('https://www.upsara.com/images/g730053_3.jpg',16,0),
                    ('https://www.upsara.com/images/q730517_4.jpg',16,0),
                    
                    ('https://www.upsara.com/images/c629594_1.jpg',17,0),
                    ('https://www.upsara.com/images/v226136_2.jpg',17,1),
                    ('https://www.upsara.com/images/w098813_3.jpg',17,0),
                    ('https://www.upsara.com/images/n142869_4.jpg',17,0),
                    
                    ('https://www.upsara.com/images/u412469_1.jpg',18,0),
                    ('https://www.upsara.com/images/g125358_2.jpg',18,0),
                    ('https://www.upsara.com/images/s985582_3.jpg',18,0),
                    ('https://www.upsara.com/images/t087487_4.jpg',18,1),
                    
                    ('https://www.upsara.com/images/r314931_1.jpg',19,0),
                    ('https://www.upsara.com/images/a094046_2.jpg',19,1),
                    ('https://www.upsara.com/images/i588431_3.jpg',19,0),
                    ('https://www.upsara.com/images/c072970_4.jpg',19,0),
                    
                    ('https://www.upsara.com/images/h512600_1.jpg',20,0),
                    ('https://www.upsara.com/images/s926424_2.jpg',20,1),
                    ('https://www.upsara.com/images/k056539_3.jpg',20,0),
                    ('https://www.upsara.com/images/i061348_4.jpg',20,0)");
        }
        
        public override void Down()
        {
        }
    }
}
